﻿<%@ page language="C#" autoeventwireup="true" inherits="Chat_Photo, App_Web_4p0w1hca" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
    <%=MainCss%>
    
    <script type="text/javascript">

        function ChkUploadImage(ct_file, ct_image) {
            var fileext = ct_file.value.substring(ct_file.value.lastIndexOf("."), ct_file.value.length);
            fileext = fileext.toLowerCase();
            if ((fileext != '.jpg') && (fileext != '.gif') && (fileext != '.jpeg') && (fileext != '.png')) {
                alert("Please upload image.");
                ct_file.focus();
                ct_file.outerHTML = ct_file.outerHTML;
            }
            else {
                ct_image.style.display = "block";
                ct_image.src = window.webkitURL.createObjectURL(ct_file.files[0])
            }
        }

    </script>

</head>
<body style="background:#F5F5F5">
    <form id="form1" runat="server">
        
        <div class="top">
            <div style="padding:0px 15px; line-height:50px; font-size:13pt;">
                头像设置
            </div>
        </div>

        <div style="padding-top:150px;">
    
            <div class="upload-wrap">
                <div class="upload-img"><asp:Image ID="imgPic" runat="server" /></div>
                <div class="upload-icon"></div>
                <asp:FileUpload ID="upLoad" runat="server" accept="image/*" CssClass="upload" onchange="ChkUploadImage(this,imgPic);" />
            </div>
        
            <div style="padding:10px; text-align:center">
                <asp:Button ID="btnSave" runat="server" Text="保存" OnClick="btnSave_Click" style="width:100px; padding:5px; background:#E9E9E9; border:solid 1px #ddd" />
            </div>
            
            <div style="padding:10px; text-align:center">
                <asp:Label ID="lblInfo" runat="server"></asp:Label>
            </div>

        </div>

    </form>
</body>
</html>
