
/*Filter*/
function strFilter1(suc) {
    var re = /\b(and|or|exec|execute|insert|select|delete|update|alter|create|drop|count|\*|chr|char|asc|mid|substring|master|truncate|declare|xp_cmdshell|restore|backup|net +user|net +localgroup +administrators)\b/;
    return suc.replace(re, '').replace(/</gi, "＜").replace(/>/gi, "＞");
}

//When page loading
function ShowLoading(obj,text)
{
    obj.value = "Loading...";
    var loadArea = document.createElement("div");
    loadArea.className = "loading-bg1";
    var html = "<div class=\"loading-bg\"></div><div class=\"loading1\">"+text+"</div>";
    loadArea.innerHTML = html;
    document.body.appendChild(loadArea);
}

document.write("<div class='loading-bg'><div class='loading'></div></div>");

window.onload = function()
{
    $(".loading-bg").hide();
}

// Get query string like 'search.aspx?id=123',name is 'id',return '123'        
function GetQueryString(name)
{
     var reg = new RegExp("(^|&)"+ name +"=([^&]*)(&|$)");
     var r = window.location.search.substr(1).match(reg);
     if(r!=null)return  unescape(decodeURI(r[2])); return null;
}

function goTop()
{
    $('html, body').animate({scrollTop:0}, 'slow');
}

function goBottom()
{
    $('html, body').animate({scrollTop: $(document).height()}, 'slow');
}

function goPosition(v)
{
    $('html, body').animate({scrollTop: v}, 0);
}

function openUrl(url)
{
    window.location.href = url;
}
        
function ShowAttention(message)
{
	$('#attention-dialog').html(
		'<p class="attention">'+message+'</p>'
	);	
	$('#attention-dialog').show();
	
	var height = $('#attention-dialog').height();
	var windowheight = $(window).height() - 50;
	//$('#attention-dialog').css('top', windowheight/2);
	$('#attention-dialog').css('top', 200);
	$('#attention-dialog').css('margin-top', -height/2);
	
	setTimeout(function(){
		$('#attention-dialog').hide();
		$('#attention-dialog').css('top', -10000);
	}, 2000);
}

function ShowMsg(text)
{
    ShowAttention(text);
}

function getLocation() {
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(showPosition);
    }
}

function showPosition(position) {
    document.getElementById('txtPosition').value = position.coords.longitude + "," + position.coords.latitude;
}

//检查上传文件类型
function ChkUpload(ct_file) {
    var fileext = ct_file.value.substring(ct_file.value.lastIndexOf("."), ct_file.value.length);
    fileext = fileext.toLowerCase();
    if ((fileext == '.exe') || (fileext == '.asp') || (fileext == '.aspx') || (fileext == '.php')) {
        alert("Illegal file type!");
        ct_file.focus();
        ct_file.outerHTML = ct_file.outerHTML;
    }
    else {
        if (document.getElementById('filepath') != null) {
            document.getElementById('filepath').value = ct_file.value.substring(ct_file.value.lastIndexOf("\\") + 1, ct_file.value.length);
        }
    }
}

//新增接收人
function setReceiver1(name, ids) {

    var txtName = window.parent.document.getElementById('txtReceiver').value;
    var txtIDs = window.parent.document.getElementById('txtReceiverID').value;

    window.parent.document.getElementById('txtReceiver').value = name;
    window.parent.document.getElementById('txtReceiverID').value = ids;

    window.parent.$.fancybox.close();
}

//追加接收人
function setReceiver2(name, ids) {

    var txtName = window.parent.document.getElementById('txtReceiver').value;
    var txtIDs = window.parent.document.getElementById('txtReceiverID').value;

    if (txtName == "" && txtIDs == "") {
        window.parent.document.getElementById('txtReceiver').value = name;
        window.parent.document.getElementById('txtReceiverID').value = ids;
    }
    else {
        window.parent.document.getElementById('txtReceiver').value = txtName + '; ' + name;
        window.parent.document.getElementById('txtReceiverID').value = txtIDs + '|' + ids;
    }

    window.parent.$.fancybox.close();
}

//新增通知人
function setNotify1(name, ids) {

    var txtName = window.parent.document.getElementById('txtNotify').value;
    var txtIDs = window.parent.document.getElementById('txtNotifyID').value;

    window.parent.document.getElementById('txtNotify').value = name;
    window.parent.document.getElementById('txtNotifyID').value = ids;

    window.parent.$.fancybox.close();
}

//追加通知人
function setNotify2(name, ids) {

    var txtName = window.parent.document.getElementById('txtNotify').value;
    var txtIDs = window.parent.document.getElementById('txtNotifyID').value;

    if (txtName == "" && txtIDs == "") {
        window.parent.document.getElementById('txtNotify').value = name;
        window.parent.document.getElementById('txtNotifyID').value = ids;
    }
    else {
        window.parent.document.getElementById('txtNotify').value = txtName + '; ' + name;
        window.parent.document.getElementById('txtNotifyID').value = txtIDs + '|' + ids;
    }

    window.parent.$.fancybox.close();
}

//新增收信人
function setTo1(address) {

    var txtAddress = window.parent.document.getElementById('txtTo').value;

    window.parent.document.getElementById('txtTo').value = address;

    window.parent.$.fancybox.close();
}

//追加收信人
function setTo2(address) {

    var txtAddress = window.parent.document.getElementById('txtTo').value;

    if (txtAddress == "") {
        window.parent.document.getElementById('txtTo').value = address;
    }
    else {
        window.parent.document.getElementById('txtTo').value = txtAddress + '; ' + address;
    }

    window.parent.$.fancybox.close();
}