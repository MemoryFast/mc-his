﻿<%@ page language="C#" autoeventwireup="true" inherits="Booking_Touch_Dashboard_Default, App_Web_0ed0zj05" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title></title>
    
    <style type="text/css">
        .icon_tb{width:100%;border-collapse:collapse;border-spacing:0;}
        .icon_tb td{ text-align:center; padding:20px 0; border:solid 1px #eee; width:33.3%}
        .icon_tb .iconfont{font-size:25pt; color:#666;}
        .icon_tb a{font-size:10pt; color:#666;}
        .copyright{text-align:center; width:100%; border-top:solid 1px #ddd; padding-top:10px;}
        .copyright a{color:#999; text-decoration:none}
        .tip{padding:10px; line-height:1.9em; color:#666;}
        .tip p{ font-size:9pt;}
    </style>
    
</head>
    
<body class="body">
    
    <header>

        <div class="topbar">
            <a href="javascript:;" class="back_btn"></a>
            <a href="javascript:;" class="top_home"></a>
            <h1 class="page_title"><asp:Label ID="lblTitle" runat="server"></asp:Label></h1>
        </div>

    </header>

    <form id="form1" runat="server">
    
        <div class="member_bg">

            <div style="height:30px"></div>

            <div style="padding-bottom:10px; text-align:center">
                <a href="Setting.aspx"><asp:Image ID="imgPortrait" runat="server" CssClass="portrait" style="border:solid 3px #fff;" /></a>
            </div>
            
            <div style="text-align:center; color:#fff;">
                <asp:Label ID="lblFullName" runat="server"></asp:Label>
            </div>

            <div style="height:40px"></div>

        </div>
    
        <div style="background:#fff;">

            <table class="icon_tb">
                <tr>
                    <td style="border-left:0px;">
                        <a href="Add.aspx"><i class="iconfont">&#xf00fc;</i><br /><span>预约挂号</span></a>
                    </td>
                    <td style="border-right:0px;">
                        <a href="List.aspx"><i class="iconfont">&#xf00b8;</i><br /><span>预约记录</span></a>
                    </td>
                </tr>
            </table>

        </div>
        
        <div class="tip">
            <div style="font-size:12pt; font-weight:700;">
                温馨提示：
            </div>
            <p>1.尽量提前预约好熟悉的医师。</p>
            <p>2.如果熟悉的医师已经满约，可选择其他医师。</p>
            <p>3.预约好医师后，没有特殊情况尽量不要爽约。</p>
            <p>4.如有紧急情况无法提前预约，请用电话联系我们。</p>
            <p>5.预约系统只作辅助使用，最终解释权归本院所有。</p>
        </div>

        <div class="copyright"><a href="http://oa.mojocube.com/" target="_blank">技术支持：魔方动力</a></div>
    
    </form>
 
</body>

</html>